echo "Get package version number"

PACKAGE_VERSION=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

echo $PACKAGE_VERSION

echo "Generate Version Badge"

npx badge-maker version $PACKAGE_VERSION :blue @flat-square > assets/version.svg

git add assets
