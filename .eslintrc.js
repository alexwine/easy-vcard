module.exports = {
  root: true,
  env: {
    browser: true,
    amd: true,
    node: true,
  },
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'prettier'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    // 'prettier',
    // 'plugin:prettier/recommended',
    'prettier/@typescript-eslint',
  ],
  rules: {
    // 'prettier/prettier': [
    //   'error',
    //   {},
    //   {
    //     usePrettierrc: true,
    //   },
    // ],
    // 'prettier/prettier': [
    //   'error': 'error',
    //   'error',
    //   {
    //     singleQuote: true,
    //     // parser: 'typescript',
    //   },
    // ],
    // quotes: ["error", "single"],
    // quotes: [
    //   'error',
    //   'single',
    //   { avoidEscape: true, allowTemplateLiterals: false },
    // ],
  },
};
