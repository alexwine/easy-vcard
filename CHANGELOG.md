# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.3.0](https://gitlab.com/alexwine/easy-vcard/compare/v1.2.2...v1.3.0) (2020-08-04)


### Features

* **formatter:** added prodid to formatter with tests ([43b6e74](https://gitlab.com/alexwine/easy-vcard/commit/43b6e742d9c34fe6582d517071a5a07253d52b8b))
* **vcard:** added prodid to vcard with test ([930893b](https://gitlab.com/alexwine/easy-vcard/commit/930893b84521b7e6441ac2a8126a4f56eb43d09d))

### [1.2.2](https://gitlab.com/alexwine/easy-vcard/compare/v1.2.1...v1.2.2) (2020-08-04)


### Bug Fixes

* **gitlab ci:** changed build artifact path ([6911b11](https://gitlab.com/alexwine/easy-vcard/commit/6911b11ab87c47ffba6e34934f6f14836a8211ad))

### [1.2.1](https://gitlab.com/alexwine/easy-vcard/compare/v1.2.0...v1.2.1) (2020-08-04)


### Bug Fixes

* **package json:** removed npmignore and added files ([9eaf0ce](https://gitlab.com/alexwine/easy-vcard/commit/9eaf0ce846999a15a573587c352daa2ad29e9730))


### Styling

* **lint:** changed linting to eslint and prettier ([4d985a1](https://gitlab.com/alexwine/easy-vcard/commit/4d985a17d562cc49a7f2eb2cb2933788696b951e))

## [1.2.0](https://gitlab.com/alexwine/easy-vcard/compare/v1.1.0...v1.2.0) (2020-08-04)


### Features

* **setrevision:** added method for using date as input ([ddfa16d](https://gitlab.com/alexwine/easy-vcard/commit/ddfa16dcd60c0cbf0a4d68650f83e44873e3292b))

## [1.1.0](https://gitlab.com/alexwine/easy-vcard/compare/v1.0.1...v1.1.0) (2020-08-04)


### Features

* **vcard:** set source function ([944956b](https://gitlab.com/alexwine/easy-vcard/commit/944956baeb4b6eb6642c9fc7d5c1a1133cb31503))


### Code Refactoring

* **clean:** removed unused folders and added coverage to jest ([56e364b](https://gitlab.com/alexwine/easy-vcard/commit/56e364b344a83b942830def7d1e65360fb0b53ce))
* **clean:** updated readme and npmignore ([cd088a5](https://gitlab.com/alexwine/easy-vcard/commit/cd088a5b15a4975f96969a86c4c1bfe508f4dfa9))

### 1.0.1 (2020-08-03)


### Bug Fixes

* **generate badge:** added generate badge script for versioning ([0e4ca0b](https://gitlab.com/alexwine/easy-vcard/commit/0e4ca0bbb3c32f2c1766464912e5ef11fe52c369))
* **generate badge:** chmod +x generateBadge ([e31f14e](https://gitlab.com/alexwine/easy-vcard/commit/e31f14eec44a1481d598034d7d6ffefda04097b1))


### CI

* **initial:** added initial gitlab ci commit ([50dee1c](https://gitlab.com/alexwine/easy-vcard/commit/50dee1c5008da807c177732f5ad9e192b1d9d994))

`0.1.0` 2020-03-05
---------------------------------------
- Add url field to vcard and formatter
- Version bump

`0.0.5` 2019-01-19
---------------------------------------
- `Fixed` gitignore preventing js files from being published

`0.0.4` 2019-01-19
---------------------------------------
- `Added` photo field
